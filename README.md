SSL Unpinnig tools.

This repository contains a set of tools that help to perform SSL Unpinning for Android aplications.

1) Apk Easy Tool is a lightweight GUI application that enables you to manage, sign, compile and decompile the APK files for the apps you are working on.

2) RootAVD a script to:
	- root your Android Studio Virtual Device (AVD), with Magisk (Stable, Canary or Alpha)
	- patch its fstab
	- download and install the USB HOST Permissions Module for Magisk
	- install custom build Kernel and its Modules
	- download and install AOSP prebuilt Kernel and its Modules
	
3) Frida server and frida-ssl-unpinning-script to debug and patch researched application.